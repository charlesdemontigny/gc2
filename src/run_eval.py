import pandas as pd
import numpy as np
import cx_Oracle


def make_dataset(connection, attribute, catbat, region, sampling_prob = None):
    """
    Load the gc2_prediction_compare dataset for a given attribute and a given combination of catbat-region.

    Args:
        connection: list
            - user in position 0
            - password in position 1
            - database in position 2
        attribute: string
            Name of the attribute we want to test. The name must be the same as the column name in IMM_IMMEUBLE if the attribut is taken from that table.
        catbat: string
            Building category we want to segment the dataset on
        region: int
            Region code we want to segment the dataset on
        sampling_prob: float
            Sampling probability E[0, 1]

    Returns: panda DataFrame
    """
    #Setup connection
    conn = cx_Oracle.connect(connection[0], connection[1], connection[2], encoding = 'utf-8')

    #Create query
    select_gc2_compare = """
                    SELECT   ID,{0},
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP0) as comp0,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP1) as COMP1,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP2) as COMP2 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP3) as COMP3,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP4) as COMP4 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP5) as COMP5 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP6) as COMP6 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP7) as COMP7 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP8) as COMP8,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP9) as COMP9 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP10) as COMP10 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP11) as COMP11 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP12) as COMP12 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP13) as COMP13 ,
                    (SELECT {0} FROM IMM_IMMEUBLE WHERE IMM_NO_ID_UEF= COMP14) as COMP14
                    FROM GC2_PREDICTION""".format(attribute)
    from_gc2_compare = """
                    JOIN IMM_IMMEUBLE im0 ON ID = im0.IMM_NO_ID_UEF
                    where im0.IMM_CAT_BAT in ('{0}')
                    and im0.IMM_NO_RA = {1}
                    """.format(catbat, region)

    if sampling_prob is not None:
        sample = "sample({})".format(int(float(sampling_prob) * 100))
        query_gc2_compare = "\n".join([select_gc2_compare, sample, from_gc2_compare])
    else:
        query_gc2_compare = "\n".join([select_gc2_compare, from_gc2_compare])

    #Convert dataset into df
    gc2_compare = pd.read_sql(query_gc2_compare, con = conn)

    # Remove row if attribute is missing
    gc2_compare[attribute].replace(0, np.nan, inplace = True)
    gc2_compare = gc2_compare.dropna(axis=0, subset=[attribute])
    assert gc2_compare[attribute].isnull().sum() == 0, "There is missings values in the {} columns".format(attribute)

    return gc2_compare

def num_metrics(df, attribute):
    """
    Compute a set of different metrics for numerical attributes.

    Args:
        df: pandas dataframe
            The dataset with the subject's ID and attribute, as well as all the comparants' attributes
        attribute: string
            Name of the attribute we want to test. The name must be the same as the column name in IMM_IMMEUBLE if the attribute
            is taken from that table.

    Returns a panda dataframe with the metrics computed for the given attribute.
    """
    def mae(row, attribute):
        """
        This function returns the mean absolute error (mae) of a specific row (i.e observation).
        y_true: the value of the subject's attribute for the attribute being tested
        y_pred: the values of the compabarables' attritube for the attribute being tested
        Note that the function is design to be use within the apply method.Thus we will not 'manually' input the required argument.

        Args:
            row: the row index. We don't have to fill that argument: the apply function will take care of it.
            attribute: string
            Name of the attribute we want to test. The name must be the same as the column name in IMM_IMMEUBLE if the attribut is taken from that table.
        """
        temp = []
        for col in df.columns[2:]:
            if (row[col] == 0 or np.isnan(row[col]) == True):
                continue
            else:
                temp.append(np.absolute(row[attribute] - row[col]))

        return np.mean(temp)

    def mape(row, attribute):
        """
        This function returns the mean absolute percentage error (mape) of a specific row (i.e observation).
        y_true: the value of the subject's attribute for the attribute being tested
        y_pred: the values of the compabarables' attritube for the attribute being tested
        Note that the function is design to be use within the apply method.Thus we will not 'manually' input the required argument.

        Args:
            row: the row index. We don't have to fill that argument: the apply function will take care of it.
            attribute: string
            Name of the attribute we want to test. The name must be the same as the column name in IMM_IMMEUBLE if the attribut is taken from that table.
        """
        temp2 = []
        for col in df.columns[2:]:
            if (row[col] == 0 or np.isnan(row[col]) == True):
                continue
            else:
                temp2.append((np.absolute((row[attribute] - row[col]) / row[attribute])))

        return (np.mean(temp2))*100

    def medape(row, attribute):
        """
        This function returns the median absolute percentage error (MedAPE) of a specific row (i.e observation).
        y_true: the value of the subject's attribute for the attribute being tested
        y_pred: the values of the compabarables' attritube for the attribute being tested
        Note that the function is design to be use within the apply method.Thus we will not 'manually' input the required argument.

        Args:
            row: the row index. We don't have to fill that argument: the apply function will take care of it.
            attribute: string
            Name of the attribute we want to test. The name must be the same as the column name in IMM_IMMEUBLE if the attribut is taken from that table.
        """
        temp3 = []
        for col in df.columns[2:]:
            if (row[col] == 0 or np.isnan(row[col]) == True):
                continue
            else:
                temp3.append((np.absolute((row[attribute] - row[col]) / row[attribute])))

        return (np.median(temp3))*100

    metrics_N = pd.DataFrame(np.array([[df.apply(mae, attribute = attribute, axis=1).mean(), df.apply(mape, attribute = attribute, axis=1).mean(), df.apply(medape, attribute = attribute, axis=1).mean()]]), index=[attribute], columns=['AVERAGE_MAE', 'AVERAGE_MAPE', 'AVERAGE_MEDAPE'])

    return metrics_N

if __name__ == '__main__':
    df = make_dataset(connection=['jlr_website', '1qaz2wsx', 'jlrorap1relv'], attribute='IMM_SUPERF_BAT', catbat='3C', region=6, sampling_prob= None)
    metric = num_metrics(df = df, attribute='IMM_SUPERF_BAT')
    import pdb; pdb.set_trace()
