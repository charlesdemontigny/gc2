
import pandas as pd
import numpy as np
import multiprocessing

from multiprocessing.pool import ThreadPool
from sklearn.neighbors import NearestNeighbors

class Generateur_comparables:
    """
    Generate comparables transactions giving similarity within features
    """
    def __init__(self, k, n_jobs = 1, normalize = True):
        """
        Initialisation of the class.

        Args:
            k: int
                The number of comparables to output.
            n_jobs: int
                The number of parallel jobs to run.
            normalize: boolean
                Perform a normalization on the DataFrames.
        """
        self._k = k
        self._n_jobs = n_jobs
        self._norm = normalize
        self._dict = dict()

    def _normalize(self, df, is_train = True):
        """
        Mean normalization on the pandas DataFrame. The formula is:
        ``normalize value = (value - mean)/(max - min)``.
        Source : https://en.wikipedia.org/wiki/Feature_scaling

        The mean, max and min are compute on the trainset only. When the predict
        set is normalize it's using the stats of the trainset.

        Args:
            df: DataFrame
                A pandas DataFrame that contains numeric value.
            is_train: Boolean
                Indicates if the df is the trainset.

        Export: A pandas DataFrame with its value normalize.
        """
        if is_train:
            self.mean = df.mean()
            self.max = df.max()
            self.min = df.min()
        return (df - self.mean)/(self.max - self.min)

    def _clean_dataframe(self, df, cols_to_keep = ['MONTANT', 'LON', 'LAT']):
        """Drops missing values and keeps only the needed columns in the dataset.

        Args:
            df: pandas DataFrame
                A DataFrame that must contains the columns in ``cols_to_keep``.
            cols_to_keep: list
                A list of columns to keep during the cleaning process.

        Returns: A pandas DataFrame.
        """
        tmp_df = df.dropna().copy()
        iduef = tmp_df['IDUEF']
        try:
            clean_df = tmp_df[cols_to_keep].copy()
        except KeyError:
            raise KeyError("All columns in ``cols_to_keep`` must be in the DataFrame.")
        return iduef, clean_df

    def fit(self, X, y = None):
        """Fit the Sklearn NearestNeighbors algorithm on ``X``."""
        self._transac = X.copy()
        _, X = self._clean_dataframe(X)
        if self._norm:
            X = self._normalize(X)
        self.nn = NearestNeighbors(n_neighbors=self._k, n_jobs = self._n_jobs)
        self.nn.fit(X)

    def _retrieve_comparables(self, comp):
        """Get comparables IDUEF for each building"""
        # retrieve comparables for a specific id
        comparable_ids = self._transac.iloc[comp,:]['IDUEF'].values
        # loop over comparables values to
        for j, comp in enumerate(comparable_ids):
            self._dict['comparable_{}'.format(j)].append(comp)

    def transform(self, newdata):
        """Query the train NearestNeighbors algorithm with the builing in newdata"""
        # Define and clean
        iduef, newdata = self._clean_dataframe(newdata)
        # Normalize
        if self._norm:
            newdata = self._normalize(newdata, is_train = False)
        # Compute comparables transactions
        comparables = self.nn.kneighbors(X = newdata, return_distance = False)
        # Define dictionnary
        self._dict = {'id':iduef.tolist()}
        for j in np.arange(0, self._k):
            self._dict['comparable_{}'.format(j)] = []
        # Loop over id_uef in order to retrieve comparables id
        for comp in comparables:
            self._retrieve_comparables(comp)
        return pd.DataFrame(self._dict)
