# -*- coding: utf-8 -*-
import os
import cx_Oracle
import pandas as pd
from datetime import datetime as dt, timedelta

def load_datasets(connection, sampling_prob = None):
    """
    Load the transaction and the building datasets from Oracle

    Args:
        connection: list
            - user in position 0
            - password in position 1
            - database in position 2
        sampling_prob: float
            Sampling probability E[0, 1]

    Returns: Two pandas DataFrames.
    """
    # Setup connection
    conn = cx_Oracle.connect(connection[0], connection[1], connection[2], encoding = 'utf-8')
    # Create queries
    select_from_transac = """
                    select tra_id_uef as iduef, tra_cat_bat as catbat, mun_no_ra as ra, tra_mnt_transaction as montant, imm_x as lon, imm_y as lat
                    from tra_transaction"""
    options_transac = """
                    left join municipalites_bsq on imm_bsq = mun_no_bsq
                    where to_char(tra_dt_publie, 'yyyy-mm-dd') > '{}'
                    and tra_cat_bat in ('2A', '2B', '2C', '3C')
                    and tra_type = 4
                    AND TRA_IND_VENTE_MULTIPLE=0
                    AND TRA_IND_VENTE_REPRISE=0
                    AND TRA_IND_VENTE_LIE=0
                    AND TRA_MNT_TRANSACTION > 100000
                    """.format((dt.now() - timedelta(548)).strftime('%Y%m%d'))
    select_from_building = """
                    select iduef, imm_cat_bat as catbat, mun_no_ra as ra, montant, imm_x as lon, imm_y as lat
                    from ev2_prediction
                    """
    options_building = """
                    left join imm_immeuble on IDUEF = imm_no_id_uef
                    left join municipalites_bsq on imm_bsq = mun_no_bsq
                    where imm_cat_bat in ('2A', '2B', '2C', '3C')
                    """
    if sampling_prob is not None:
        sample = "sample({})".format(int(float(sampling_prob) * 100))
        query_transac = "\n".join([select_from_transac, sample, options_transac])
        query_building = "\n".join([select_from_building, sample, options_building])
    else:
        query_transac = "\n".join([select_from_transac, options_transac])
        query_building = "\n".join([select_from_building, options_building])
    # Load datasets into df
    transac = pd.read_sql(query_transac.encode('utf-8'), con = conn)
    building = pd.read_sql(query_building.encode('utf-8'), con = conn)
    # Remove duplicated IDUEF
    building.drop_duplicates('IDUEF', inplace = True)
    return transac, building


if __name__ == '__main__':
    transac, building = load_datasets()
    import pdb; pdb.set_trace()
