# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import time
import multiprocessing
import click
import os
import cx_Oracle

from glob import glob
from multiprocessing.pool import ThreadPool
from itertools import product

from generateur_comparables import Generateur_comparables
from make_dataset import load_datasets

def comp_by_region_catbat(ra, catbat):
    """
    Computes comparables transactions after having filter the datasets.

    Args:
        filter: tupple
            A tupple with the RA in position 0 and catbat in position 1.
        transac: pandas DataFrame
            The dataset with all the transactions for the past 18 months.
        building: pandas DataFrame
            The dataset with every building in the dataset.

    Returns: A pandas DataFrame with a line for each building and with k + 1 col.
    """
    print(ra, catbat)
    # filter datasets
    query = "CATBAT == '{0}' & RA == {1}".format(catbat, ra)
    tmp_transac = Parameters.transac.query(query)
    if tmp_transac.shape[0] < Parameters.k:
        tmp_transac = Parameters.transac.query("RA == {}".format(ra))
        if tmp_transac.shape[0] < Parameters.k:
            return
    tmp_building = Parameters.building.query(query)
    if tmp_building.shape[0] == 0:
        return
    # Check for duplicated ID
    assert len(set(tmp_building.IDUEF)) == tmp_building.shape[0]
    # Generate comparables DataFrame
    gen_comp = Generateur_comparables(Parameters.k, n_jobs = Parameters.n_jobs)
    gen_comp.fit(tmp_transac)
    df = gen_comp.transform(tmp_building)
    return df

class Parameters(object):
    transac = None
    building = None
    k = None
    n_jobs = None

@click.command()
@click.option('-k', '--k', default = 15)
@click.option('-n', '--n_jobs', default = 1)
@click.option('-e', '--env', default = 'dev')
@click.option('-s', '--sampling_prob', default = None)
def run_gc2(k, n_jobs, env, sampling_prob):
    """Generate comparables transactions for every building in the JLR's dataset

    Args:
        k: int
            Number of comparables transactions to output.
        n_jobs: int
            Number of CPU cores to use.
        env: str
            The environment in which the model in run, dev or prod.
        sampling_prob: float
            The sampling probability to apply on the dataset. It must be [0, 1].

    Export:
        gc2: CSV
            A CSV file that containts ID and comparables transactions.
    """
    # config
    if env == 'dev':
        db_name = 'jlrorap1relv'
        output_dir = 'data/test/'
    if env == 'prod':
        db_name = 'azuoraprod'
        output_dir = "//azuorap3/chargement/"
    connection = ['jlr_website', '1qaz2wsx', db_name]
    # Set parameters
    Parameters.k = int(k)
    Parameters.n_jobs = int(n_jobs)
    # Load dataset from Oracle database
    print("Load dataset...")
    Parameters.transac, Parameters.building = load_datasets(connection, sampling_prob)
    # Set parameters
    ra = np.arange(1, 18)
    catbat = ('2A', '2B', '2C', '3C')
    # Loop over every options in the dataset
    list_of_df = []
    for ra, catbat in product(ra, catbat):
        list_of_df.append(comp_by_region_catbat(ra, catbat))
    comparables = pd.concat(list_of_df)
    # Export the result as a CSV file
    try:
        os.makedirs(output_dir)
    except FileExistsError:
        pass
    comparables.to_csv(os.path.join(output_dir, "gc2.csv"), index = False)
    # delete trace file
    trace_files = glob("*.trc")
    try:
        [os.remove(file) for file in trace_files]
    except FileNotFoundError:
        pass
    # SQL Proc
    if env == 'prod':
        connection_export = cx_Oracle.connect('jlr_website', '1qaz2wsx', 'jlrorap1prod')
        proc = cx_Oracle.Cursor(connection_export)
        proc.callproc('jlr_website.INSERT_gc2_prediction')

if __name__ == '__main__':
    run_gc2()
