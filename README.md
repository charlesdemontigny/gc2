gc2
==============================

Getting started
---------------
Clone the repository, open a command line prompt at the project root directory.

## Setup

If needed, install virtualenvwrapper-win run in a command line prompt:

```
pip install virtualenvwrapper-win
```

For setup, create the python `gc2` environment:
```
mkvirtualenv gc2
pip install -r requirements.txt
```

## Vanilla run

To run the program:
```
python src/run_gc2.py
```

Project Organization
------------

    ├── README.md                     <- The top-level README for developers using this project.
    ├── data
    │   └── test                      <- Output
    │
    ├── docs                          <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── notebooks                     <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                                   the creator's initials, and a short `-` delimited description, e.g.
    │                                   `1.0-jqp-initial-data-exploration`.
    │
    ├── references                    <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports                       <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures                   <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt              <- The requirements file for reproducing the analysis environment, e.g.
    │                                     generated with `pip freeze > requirements.txt`
    │
    └── src                           <- Source code for use in this project.
        ├── __init__.py               <- Makes src a Python module
        │
        ├── make_dataset.py           <- Scripts to download or generate data
        │
        ├── generateur_comparables.py <- Scripts where the core class of gc2 is.
        │
        └── run_gc2.py                <- Scripts to run the program.
