select iduef, imm_cat_bat as catbat, mun_no_ra as ra, montant, imm_x as lon, imm_y as lat, imm_superf_bat as superf, imm_an_construction as an_const, imm_valeur_tot as eval_mun
  from ev2_prediction
  left join imm_immeuble on IDUEF = imm_no_id_uef
  left join municipalites_bsq on imm_bsq = mun_no_bsq
  where imm_cat_bat in ('2A', '3C')
  and IMM_VALEUR_T is not null
  and IMM_VALEUR_B is not null
  and IMM_VALEUR_T > 0
  and IMM_VALEUR_B > 0