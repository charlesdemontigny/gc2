select *
from
  (select ROW_NUMBER() over (partition by tra_id_uef order by tra_dt_publie asc) as RowNo,
     tra_id_uef as iduef, tra_numero as numero, tra_cat_bat as catbat, mun_no_ra as ra, imm_bsq_nom as ville, tra_dt_publie, tra_mnt_transaction as montant, t.x as lon, t.y as lat, 
     imm_superf_bat as superf, imm_an_construction as an_const, imm_valeur_tot as eval_mun
      from
       jlr_website.tra_transaction,
       jlrprod.qc_ad_16,
       jlrprod.municipalites_bsq,
      adresse_qc.lim_reg_administratives,
      TABLE (sdo_util.getvertices(SDO_GEOM.SDO_CENTROID(qc_ad_16.geoloc, 0.005))) t

      WHERE tra_dt_publie >= TO_DATE('20171209', 'yyyymmdd')
      AND tra_dt_publie <= TO_DATE('20190610', 'yyyymmdd')
      AND tra_cat_bat in ('2A','3C')
      AND tra_type = 4
      AND tra_ind_vente_multiple = 0
      AND tra_ind_vente_reprise = 0
      AND tra_ind_vente_lie = 0
      AND tra_mnt_transaction > 100000
      AND (imm_x IS NULL OR imm_y IS NULL)
      AND imm_bsq = jlrprod.municipalites_bsq.mun_no_bsq      --JOIN municipalites_bsq
      AND mun_no_ra = codereg       --JOIN adresse_qc.lim_reg_administratives
      AND qc_ad_16.adidu = imm_ad   --JOIN qc_ad_16 left join municipalites_bsq on imm_bsq = mun_no_bsq
      and IMM_VALEUR_T is not null
      and IMM_VALEUR_B is not null
      and IMM_VALEUR_T > 0
      and IMM_VALEUR_B > 0

      ) R

      where R.RowNo = 1

union
      
select *
from
  (select ROW_NUMBER() over (partition by tra_id_uef order by tra_dt_publie asc) as RowNo,
   tra_id_uef as iduef, tra_numero as numero, tra_cat_bat as catbat, mun_no_ra as ra, imm_bsq_nom as ville, tra_dt_publie, tra_mnt_transaction as montant, imm_x as lat, imm_y as lon,
   imm_superf_bat as superf, imm_an_construction as an_const, imm_valeur_tot as eval_mun
    from jlr_website.tra_transaction
     left join jlr_website.municipalites_bsq on imm_bsq = mun_no_bsq
     WHERE tra_dt_publie >= TO_DATE('20171209', 'yyyymmdd')
     AND tra_dt_publie <= TO_DATE('20190610', 'yyyymmdd')
     and tra_cat_bat in ('2A', '2B', '2C', '3C')
     and tra_type = 4
     AND TRA_IND_VENTE_MULTIPLE=0
     AND TRA_IND_VENTE_REPRISE=0
     AND TRA_IND_VENTE_LIE=0
     AND TRA_MNT_TRANSACTION > 100000
     AND (imm_x IS NOT NULL AND imm_y IS NOT NULL)
     and IMM_VALEUR_T is not null
     and IMM_VALEUR_B is not null
     and IMM_VALEUR_T > 0
     and IMM_VALEUR_B > 0
     ) P
     where P.RowNo = 1
   
